data= read.csv('USElection2020.csv')
str(data)
sub= c('VEP.Turnout.Rate','Voting.Eligible.Population..VEP.')
df= data[sub]
head(df)
names(df)=c('VEP_Turnout_Rate','Voting_Eligible_Population_VEP.')
# cleaning the data by removing the commas
df$VEP_Turnout_Rate<- as.numeric(gsub("%", "",df$VEP_Turnout_Rate ))
df$Voting_Eligible_Population_VEP.<- as.numeric(gsub(",", "", df$Voting_Eligible_Population_VEP.)) 
head(df)
# scatter plot
plot(jitter(df$Voting_Eligible_Population_VEP.,1),df$VEP_Turnout_Rate,xlab="VEP Turnout Rate",ylab="Voting eligible population", main="VEP Turnout Rate vs Voting eligible population")
abline(lm(df$VEP_Turnout_Rate ~ df$Voting_Eligible_Population_VEP.)) 
cor(VEP_Turnout_Rate,Voting_Eligible_Population_VEP.)
# hypothesis testing
model= lm(VEP_Turnout_Rate~Voting_Eligible_Population_VEP.)
summary(model)

